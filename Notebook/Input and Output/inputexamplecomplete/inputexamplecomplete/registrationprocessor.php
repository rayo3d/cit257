<?php
    require_once 'iohelpers/input.php';

    try {
        $input = new Input();
        
        $title = $input->Text('title', 'Please select a title');
        $fname = $input->Text('fname', 'Please enter a first name');
        $lname = $input->Text('lname', 'Please enter a last name');
        $position = $input->Text('position', 'Please select a position');
        $seminar = $input->Text('seminar', 'Please select a seminar');
        $email = $input->Email('email', 'Please enter a valid email address');
        $tickets = $input->Integer('tickets', 'Please enter number of tickets', 1);
        $payment = $input->Number('payment', 'Please enter a payment amount', 0);
        $date = $input->Date('date', 'Please enter a date in the format "mm/dd/yyyy"');
        $website = $input->URL('website', 'Please enter a valid web site URL');
        if ($input->IsEmpty('mname')) {
            $mname = '';
        } else {
            $mname = $input->Text('mname');
        }
        
        if ($input->Exists('status')) {
            $status = $input->Text('status');
        } else {
            $status = 'parttime';
        }
        
        if (!empty($input->Errors)) {
            print $input->FormattedErrMsg();
            exit();
        }
        
        exit ("Registered");
        
    } catch (Exception $ex) {
        exit ($ex->getMessage() . ': ' . $ex->getCode());
    }
	
?>
