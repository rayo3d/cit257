<?php
    class Input {
        //Version 6

        /* 
         * Exception Codes
         * 
         * 	9001	Missing Input Array
         * 	9002	Input Field does not exist
         * 	9003	Value below minimum
         * 	9004	Invalid empty value
         * 	9005	Invalid value
         * 	9999	Unknown filter failure
         */

        private $accumulateErrors = TRUE;

        public $Errors = array();
        private $error = array();

        public function __construct ($accumulateErrors = TRUE) {

            if ($accumulateErrors === FALSE) {
                    $this->accumulateErrors = FALSE;
            }
        }

        private function getInputType () {
            if (!empty($_POST)) {
                    return INPUT_POST;
            } elseif (!empty($_GET)) {
                    return INPUT_GET;
            } else {
                    throw new Exception('Missing Input Array', 9001);
            }
        }

        private function getErrorMsg ($errMsg, $default) {

            if (!empty($errMsg)) {
                    return $errMsg;
            } else {
                    return $default;
            }
        }

        private function processException(Exception $e) {

            $errCode = $e->getCode();

            //Don't accumulate these errors
            if ($errCode == 9001 || $errCode == 9999) {
                    throw $e;
            }

            if ($this->accumulateErrors) {
                    $error['message'] = $e->getMessage();
                    $error['code'] = $e->getCode();
                    $this->Errors[] = $error;
            } else {
                    throw $e;
            }
        }

        public function FormattedErrMsg () {

            $formattedErrMsg = '';

            if (!empty($this->Errors)) {
                    foreach ($this->Errors as $error) {
                            $formattedErrMsg=$formattedErrMsg . $error['message'] .  '<br />';
                    }
            }
            return $formattedErrMsg;
        }

        public function MissingInputArray() {

            if (empty($_POST) && empty($_GET)) {
                    return TRUE;
            } else {
                    return FALSE;
            }
        }
        public function Exists ($field) {

            $inputType = $this->getInputType();
            $value = filter_input($inputType, $field);

            if ($value === null) {
                    return FALSE;
            } else {
                    return TRUE;
            }
        }

        public function IsEmpty ($field) {

            $inputType = $this->getInputType();
            $value = filter_input($inputType, $field);

            //If the input field does not exist 
            //OR if the field is empty, return true
            if ($value === null || $value === '') {
                    return TRUE;
            } else {
                    return FALSE;
            }
        }

        public function Text ($field, $errMsg='') {

            try {
                $stripTags=TRUE;
                $trimSpaces=TRUE;
                $minSize=1;
                $message =  $this->getErrorMsg($errMsg, $field . ' is not text');

                $inputType = $this->getInputType();
                $value = filter_input($inputType, $field);

                if ($value === null) {
                        throw new Exception($message, 9002);
                }

                if ($value === false) {
                        throw new Exception($message, 9999);
                }

                if (get_magic_quotes_gpc()){
                        //Remove any slashes (escapes) added by PHP's Magic Quotes feature (deprecated)
                        $value = stripslashes($value);
                }

                if ($stripTags) {
                        //Remove any HTML tags
                        $value = strip_tags($value);
                }

                if ($trimSpaces) {
                        //Remove leading and trailing spaces
                        $value = trim($value);
                }

                if ($value == '' && $minSize > 0) {
                        throw new Exception($message, 9004);
                }

                return $value;

            } catch (Exception $e) {
                    $this->processException($e);
            }
        }

        public function Integer ($field, $errMsg='', $minValue=null) {

            try {
                $message = $this->getErrorMsg($errMsg, $field . ' is not an integer');

                $inputType = $this->getInputType();

                $value = filter_input($inputType, $field, FILTER_VALIDATE_INT);

                if ($value === null) {
                        throw new Exception($message, 9002);
                }

                if ($value === FALSE) {
                        If ($this->IsEmpty($field)) {
                                throw new Exception($message, 9004);
                        } else {
                                throw new Exception($message, 9005);
                        }
                }

                $value = (int)$value;

                if ($value < $minValue) {
                        throw new Exception($message, 9003);
                }

                return $value;

            } catch (Exception $e) {
                    $this->processException($e);
            }
        }

        public function Number ($field, $errMsg='', $minValue=null) {

            try {
                $message =  $this->getErrorMsg($errMsg, $field . ' is not a number');

                $inputType = $this->getInputType();

                $value = filter_input($inputType, $field, FILTER_VALIDATE_FLOAT);

                if ($value === null) {
                        throw new Exception($message, 9002);
                }

                if ($value === FALSE) {
                        If ($this->IsEmpty($field)) {
                                throw new Exception($message, 9004);
                        } else {
                                throw new Exception($message, 9005);
                        }
                }

                $value = (float)$value;

                if ($value < $minValue) {
                        throw new Exception($message, 9003);
                }

                return $value;

            } catch (Exception $e) {
                    $this->processException($e);
            }
        }

        public function Email ($field, $errMsg='') {

            try {
                $message =  $this->getErrorMsg($errMsg, $field . ' is not an email address');

                $inputType = $this->getInputType();

                $value = filter_input($inputType, $field, FILTER_VALIDATE_EMAIL);

                if ($value === null) {
                        throw new Exception($message, 9002);
                }

                if ($value === FALSE) {
                        If ($this->IsEmpty($field)) {
                                throw new Exception($message, 9004);
                        } else {
                                throw new Exception($message, 9005);
                        }
                }

                return $value;
            } catch (Exception $e) {
                    $this->processException($e);
            }
        }

        public function URL ($field, $errMsg='') {

            try {
                $message =  $this->getErrorMsg($errMsg, $field . ' is not a URL');

                $inputType = $this->getInputType();

                $value = filter_input($inputType, $field, FILTER_VALIDATE_URL);

                if ($value === null) {
                        throw new Exception($message, 9002);
                }

                if ($value === FALSE) {
                        If ($this->IsEmpty($field)) {
                                throw new Exception($message, 9004);
                        } else {
                                throw new Exception($message, 9005);
                        }
                }

                return $value;
            } catch (Exception $e) {
                $this->processException($e);
            }
        }

        public function Date ($field, $errMsg='') {

            try {
                $message =  $this->getErrorMsg($errMsg, $field . ' is not a date');

                $inputType = $this->getInputType();

                If ($this->IsEmpty($field)) {
                        throw new Exception($message, 9004);
                }			

                $value = filter_input($inputType, $field);

                if ($value === null) {
                        throw new Exception($message, 9002);
                }

                if ($value === false) {
                        throw new Exception($message, 9999);
                }

                If ($this->IsEmpty($field)) {
                        throw new Exception($message, 9004);
                }

                $dateParts = explode('/', $value);
                If (count($dateParts) < 3) {
                        throw new Exception($message, 9005);
                }

                if (!checkdate($dateParts[0], $dateParts[1], $dateParts[2])) {
                        throw new Exception($message, 9005);
                }

                return $value;
            } catch (Exception $e) {
                    $this->processException($e);
            }
        }
    }

?>

