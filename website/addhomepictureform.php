<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kentucky Lake Homes</title>
        <link href="templates/css/mainstyle.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page">
            <?php
                session_start();

                //If the username is not in the $_SESSION array
                if (empty($_SESSION['username'])) {
                    //Exit with an error message
                    exit("You must be logged in to perform this function.");
                }

                //If the username is not 'admin'
                if ($_SESSION['username'] != 'admin') { 
                    //Exit with an error message
                    exit ("You are not allowed to perform this function");
                }

                require_once 'iohelpers/input.php';

                try {
                    // Get homeid (minimum accetable value of 1
                    $input = new Input();
                    $homeid = $input->Integer('homeid', 'Invalid homeid', 1);

                    if (!empty($input->Errors)) {
                        exit ('Invalid homeid');
                    }

                    // Transfer control to a page that will display the pictures upload form
                    include 'view/displayaddhomepictureform.php';
                    exit;
                } catch (Exception $ex) {
                    exit('Update Home Pictures Exception: '
                        . $ex->getMessage());
                }
            ?>
        </div>
    </body>
</html>