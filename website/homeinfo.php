<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kentucky Lake Homes</title>
        <link href="templates/css/mainstyle.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page">
            <?php
                require_once 'model/homesDB.php';
                require_once 'iohelpers/input.php';
                
                try {
                    // Get homeid (minimum accetable value of 1
                    $input = new Input();
                    $homeid = $input->Integer('homeid', 'Invalid homeid', 1);
                    
                    if (!empty($input->Errors)) {
                        exit ('Invalid homeid');
                    }
                    
                    // Retrieve the home with the matching homeid
                    $home = getHomeInfo($homeid);
                    
                    // Transfer control to a page that will display the homes
                    include 'view/displayhomesinfo.php';
                    exit;
                } catch (Exception $ex) {
                    exit('Home Info Search Exception: '
                        . $ex->getMessage());
                }
            ?>
        </div>
    </body>
</html>
