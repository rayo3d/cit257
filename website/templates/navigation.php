<!doctype html>
<html>
	<head>
		<title></title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width-device-width, initial-scale-1.0">
		<link href="templates/css/mainstyle.css" rel="stylesheet" type="text/css">
	</head>
    
    <body>
		<!-- Start Navigation -->
        <div id="navigation" class="nav">
            <h1>Site Navigation</h1>
            <ui>
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Sign In</a></li>
                <li><a href="addhomeform.php">Add a Home</a></li>
                <li><a href="#">Search</a></li>
            </ui>
            
            <?php
                session_start();
            
                if (!empty($_SESSION['username'])) {
                    $username = $_SESSION['username'];
                    print '<div id="loginSide" class="loginside loggedin">
                            <h1>Logged In</h1>
                            <p>You are logged in as user<br>' . $username . 
                            '</p></div>';
                } else {
                    print '<div id="loginSide" class="loginside">
                            <h1>Log In</h1>
                            <form action="login.php" method="post">
                                <label for="username">Username:</label><input type="text" name="username"><br>
                                <label for="password">Password:</label><input type="password" name="password"><br><br>
                                <input type="submit" name="submit" value="Submit">
                            </form>
                            </div>';
                }
            ?>
            
            <h1>Lake Information</h1>
            <ui>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/barren-river-lake/6/">Barren River Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/buckhorn-lake/15/">Buckhorn Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/cave-run-lake/7/">Cave Run Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/dale-hollow-lake/5/">Dale Hollow Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/dewey-lake/18/">Dewey Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/fishtrap-lake/17/">Fishtrap Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/grayson-lake/14/">Grayson Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/green-river-lake/8/">Green River Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/herrington-lake/12/">Herrington Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/kentucky-lake/1/">Kentucky Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/lake-barkley/3/">Lake Barkley</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/lake-cumberland/4/">Lake Cumberland</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/laurel-river-lake/9/">Laurel River Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/nolin-lake/10/">Nolin Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/paintsville-lake/16/">Paintsville Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/rough-river-lake/11/">Rough River Lake</a></li>
                <li><a href="http://www.kentuckytourism.com/lakes_rivers/taylorsville-lake/13/">Taylorsville Lake</a></li>
            </ui>
        </div>
        <!-- Start Navigation -->
    </body>
</html>