<!doctype html>
<html>
	<head>
            <title>
                <?php
                    if (isset($title)) {
                        print $title; 
                    } else {
                        print 'Kentucky Lake Homes';
                    }
                ?>
            </title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width-device-width, initial-scale-1.0">
            <link href="templates/css/mainstyle.css" rel="stylesheet" type="text/css">
	</head>

    <body>
		<!-- Start Header -->
        <div id="header" class="header">
            Kentucky Lake Homes
        </div>
        <!-- End Header -->
    </body>
</html>