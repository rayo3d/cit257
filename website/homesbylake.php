<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kentucky Lake Homes</title>
        <link href="templates/css/mainstyle.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page">
            <?php
                require_once 'model/lakesDB.php';
                require_once 'model/homesDB.php';
                require_once 'iohelpers/input.php';
                
                try {
                    // Get lakeid (minimum accetable value of 1
                    $input = new Input();
                    $lakeID = $input->Integer('lakeid', 'Invalid lakeid', 1);
                    
                    if (!empty($input->Errors)) {
                        exit ('Invalid lakeid');
                    }
                    
                    // Retrieve the lake with the matching lakeid
                    $lakeNames = getLake($lakeID);
                    $lakeName = end($lakeNames);

                    // Get all homes from the homes table
                    $homes = getHomesByLakes_lakeid($lakeID);
                    
                    // Transfer control to a page that will display the homes
                    include 'view/displayhomesbylake.php';
                    exit;
                } catch (Exception $ex) {
                    exit('Home by Lake Search Exception: '
                        . $ex->getMessage());
                }
            ?>
        </div>
    </body>
</html>
