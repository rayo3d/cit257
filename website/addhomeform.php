<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kentucky Lake Homes</title>
        <link href="templates/css/mainstyle.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page">
            <?php
                session_start();
                
                //If the username is not in the $_SESSION array
                if (empty($_SESSION['username'])) {
                    //Exit with an error message
                    exit("You must be logged in to perform this function.");
                }
                
                //If the username is not 'admin'
                if ($_SESSION['username'] != 'admin') { 
                    //Exit with an error message
                    exit ("You are not allowed to perform this function");
                }
            
                require_once 'model/lakesDB.php';
                require_once 'model/locationsDB.php';
                require_once 'model/sellersDB.php';
                
                try {                    
                    $lakes = getLakes(); // Retrieve all lakes
                    $locations = getLocations(); // Retrieve all locations
                    $sellers = getSellers(); // Retrieve all sellers
                    
                    // Transfer control to a page that will display the homes
                    include 'view/displayaddhomeform.php';
                    exit;
                } catch (Exception $ex) {
                    exit('Home Info Search Exception: '
                        . $ex->getMessage());
                }
            ?>
        </div>
    </body>
</html>
