<?php
	require_once 'model/connectionDB.php';

	function getLake ($lakeid) {

		$db = getConnection();
		$query = 'Select * From lakes Where lakeid = :lakeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':lakeid', $lakeid);
		$statement->execute();
		$lake = $statement->fetch();
		$statement->closeCursor();
		return $lake;
	}

	function updateLake ($lakeid, $lakename) {

		$db = getConnection();
		$query = 'Update lakes Set lakename = :lakename
			 Where lakeid = :lakeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':lakename', $lakename);
		$statement->bindValue(':lakeid', $lakeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function addLake ($lakename) {

		$db = getConnection();
		$query = 'Insert Into lakes (lakename)
			 Values (:lakename)';
		$statement = $db->prepare($query);
		$statement->bindValue(':lakename', $lakename);
		$statement->execute();
		$statement->closeCursor();
	}

	function deleteLake ($lakeid) {

		$db = getConnection();
		$query = 'Delete From lakes Where lakeid = :lakeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':lakeid', $lakeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function getLakes () {

		$db = getConnection();
		$query = 'Select * From lakes';
		$statement = $db->prepare($query);
		$statement->execute();
		$lakes = $statement->fetchAll();
		$statement->closeCursor();
		return $lakes;
	}

?>

