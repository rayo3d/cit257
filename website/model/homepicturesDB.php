<?php
	require_once 'model/connectionDB.php';

	function getHomepic ($homepicturesid, $homes_homeid) {

		$db = getConnection();
		$query = 'Select * From homepictures Where homepicturesid = :homepicturesid And homes_homeid = :homes_homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':homepicturesid', $homepicturesid);
		$statement->bindValue(':homes_homeid', $homes_homeid);
		$statement->execute();
		$homepic = $statement->fetch();
		$statement->closeCursor();
		return $homepic;
	}

	function updateHomepic ($homepicturesid, $homepicture, $homepiclink, $homes_homeid) {

		$db = getConnection();
		$query = 'Update homepictures Set homepicture = :homepicture, homepiclink = :homepiclink
			 Where homepicturesid = :homepicturesid And homes_homeid = :homes_homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':homepicture', $homepicture);
		$statement->bindValue(':homepiclink', $homepiclink);
		$statement->bindValue(':homepicturesid', $homepicturesid);
		$statement->bindValue(':homes_homeid', $homes_homeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function addHomepic ($homepicture, $homepiclink, $homes_homeid) {

		$db = getConnection();
		$query = 'Insert Into homepictures (homepicture, homepiclink, homes_homeid)
			 Values (:homepicture, :homepiclink, :homes_homeid)';
		$statement = $db->prepare($query);
		$statement->bindValue(':homepicture', $homepicture);
		$statement->bindValue(':homepiclink', $homepiclink);
		$statement->bindValue(':homes_homeid', $homes_homeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function deleteHomepic ($homepicturesid, $homes_homeid) {

		$db = getConnection();
		$query = 'Delete From homepictures Where homepicturesid = :homepicturesid And homes_homeid = :homes_homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':homepicturesid', $homepicturesid);
		$statement->bindValue(':homes_homeid', $homes_homeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function getHomepictures () {

		$db = getConnection();
		$query = 'Select * From homepictures';
		$statement = $db->prepare($query);
		$statement->execute();
		$homepictures = $statement->fetchAll();
		$statement->closeCursor();
		return $homepictures;
	}


	function getHomepicturesByHomes_homeid ($homes_homeid) {

		$db = getConnection();
		$query = 'Select * From homepictures
			 Where homes_homeid = :homes_homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':homes_homeid', $homes_homeid);
		$statement->execute();
		$homepictures = $statement->fetchAll();
		$statement->closeCursor();
		return $homepictures;
	}

?>

