<?php
	require_once 'model/connectionDB.php';

	function getLocation ($locationid) {

		$db = getConnection();
		$query = 'Select * From locations Where locationid = :locationid';
		$statement = $db->prepare($query);
		$statement->bindValue(':locationid', $locationid);
		$statement->execute();
		$location = $statement->fetch();
		$statement->closeCursor();
		return $location;
	}

	function updateLocation ($locationid, $location) {

		$db = getConnection();
		$query = 'Update locations Set location = :location
			 Where locationid = :locationid';
		$statement = $db->prepare($query);
		$statement->bindValue(':location', $location);
		$statement->bindValue(':locationid', $locationid);
		$statement->execute();
		$statement->closeCursor();
	}

	function addLocation ($location) {

		$db = getConnection();
		$query = 'Insert Into locations (location)
			 Values (:location)';
		$statement = $db->prepare($query);
		$statement->bindValue(':location', $location);
		$statement->execute();
		$statement->closeCursor();
	}

	function deleteLocation ($locationid) {

		$db = getConnection();
		$query = 'Delete From locations Where locationid = :locationid';
		$statement = $db->prepare($query);
		$statement->bindValue(':locationid', $locationid);
		$statement->execute();
		$statement->closeCursor();
	}

	function getLocations () {

		$db = getConnection();
		$query = 'Select * From locations';
		$statement = $db->prepare($query);
		$statement->execute();
		$locations = $statement->fetchAll();
		$statement->closeCursor();
		return $locations;
	}

?>

