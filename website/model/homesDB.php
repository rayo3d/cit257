<?php
	require_once 'model/connectionDB.php';

	function getHome ($homeid) {

		$db = getConnection();
		$query = 'Select * From homes Where homeid = :homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':homeid', $homeid);
		$statement->execute();
		$home = $statement->fetch();
		$statement->closeCursor();
		return $home;
	}
        
        function getHomeInfo ($homeid) {

		$db = getConnection();
		$query = 'Select
                            homes.address,
                            homes.city,
                            homes.state,
                            homes.zip,
                            homes.description,
                            homes.bedrooms,
                            homes.bathrooms,
                            homes.squarefeet,
                            homes.price,
                            locations.location,
                            sellers.firstname,
                            sellers.lastname,
                            sellers.company,
                            lakes.lakename
                          From
                            homes Inner Join
                            lakes
                              On homes.lakes_lakeid = lakes.lakeid Inner Join
                            locations
                              On homes.locations_locationid = locations.locationid Inner Join
                            sellers
                              On homes.sellers_sellerid = sellers.sellerid
                          Where homeid = :homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':homeid', $homeid);
		$statement->execute();
		$home = $statement->fetch();
		$statement->closeCursor();
		return $home;
	}

	function updateHome ($homeid, $address, $city, $state, $zip, $description, $bedrooms, $bathrooms, $squarefeet, $price, $lake, $location, $seller, $locations_locationid, $sellers_sellerid, $lakes_lakeid) {

		$db = getConnection();
		$query = 'Update homes Set address = :address, city = :city, state = :state, zip = :zip, description = :description, bedrooms = :bedrooms, bathrooms = :bathrooms, squarefeet = :squarefeet, price = :price, lake = :lake, location = :location, seller = :seller, locations_locationid = :locations_locationid, sellers_sellerid = :sellers_sellerid, lakes_lakeid = :lakes_lakeid
			 Where homeid = :homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':address', $address);
		$statement->bindValue(':city', $city);
		$statement->bindValue(':state', $state);
		$statement->bindValue(':zip', $zip);
		$statement->bindValue(':description', $description);
		$statement->bindValue(':bedrooms', $bedrooms);
		$statement->bindValue(':bathrooms', $bathrooms);
		$statement->bindValue(':squarefeet', $squarefeet);
		$statement->bindValue(':price', $price);
		$statement->bindValue(':lake', $lake);
		$statement->bindValue(':location', $location);
		$statement->bindValue(':seller', $seller);
		$statement->bindValue(':locations_locationid', $locations_locationid);
		$statement->bindValue(':sellers_sellerid', $sellers_sellerid);
		$statement->bindValue(':lakes_lakeid', $lakes_lakeid);
		$statement->bindValue(':homeid', $homeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function addHome ($address, $city, $state, $zip, $description, $bedrooms, $bathrooms, $squarefeet, $price, $lake, $location, $seller, $locations_locationid, $sellers_sellerid, $lakes_lakeid) {

		$db = getConnection();
		$query = 'Insert Into homes (address, city, state, zip, description, bedrooms, bathrooms, squarefeet, price, lake, location, seller, locations_locationid, sellers_sellerid, lakes_lakeid)
			 Values (:address, :city, :state, :zip, :description, :bedrooms, :bathrooms, :squarefeet, :price, :lake, :location, :seller, :locations_locationid, :sellers_sellerid, :lakes_lakeid)';
		$statement = $db->prepare($query);
		$statement->bindValue(':address', $address);
		$statement->bindValue(':city', $city);
		$statement->bindValue(':state', $state);
		$statement->bindValue(':zip', $zip);
		$statement->bindValue(':description', $description);
		$statement->bindValue(':bedrooms', $bedrooms);
		$statement->bindValue(':bathrooms', $bathrooms);
		$statement->bindValue(':squarefeet', $squarefeet);
		$statement->bindValue(':price', $price);
		$statement->bindValue(':lake', $lake);
		$statement->bindValue(':location', $location);
		$statement->bindValue(':seller', $seller);
		$statement->bindValue(':locations_locationid', $locations_locationid);
		$statement->bindValue(':sellers_sellerid', $sellers_sellerid);
		$statement->bindValue(':lakes_lakeid', $lakes_lakeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function deleteHome ($homeid) {

		$db = getConnection();
		$query = 'Delete From homes Where homeid = :homeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':homeid', $homeid);
		$statement->execute();
		$statement->closeCursor();
	}

	function getHomes () {

		$db = getConnection();
		$query = 'Select * From homes';
		$statement = $db->prepare($query);
		$statement->execute();
		$homes = $statement->fetchAll();
		$statement->closeCursor();
		return $homes;
	}


	function getHomesByLakes_lakeid ($lakes_lakeid) {

		$db = getConnection();
		$query = 'Select * From homes
			 Where lakes_lakeid = :lakes_lakeid';
		$statement = $db->prepare($query);
		$statement->bindValue(':lakes_lakeid', $lakes_lakeid);
		$statement->execute();
		$homes = $statement->fetchAll();
		$statement->closeCursor();
		return $homes;
	}


	function getHomesByLocations_locationid ($locations_locationid) {

		$db = getConnection();
		$query = 'Select * From homes
			 Where locations_locationid = :locations_locationid';
		$statement = $db->prepare($query);
		$statement->bindValue(':locations_locationid', $locations_locationid);
		$statement->execute();
		$homes = $statement->fetchAll();
		$statement->closeCursor();
		return $homes;
	}


	function getHomesBySellers_sellerid ($sellers_sellerid) {

		$db = getConnection();
		$query = 'Select * From homes
			 Where sellers_sellerid = :sellers_sellerid';
		$statement = $db->prepare($query);
		$statement->bindValue(':sellers_sellerid', $sellers_sellerid);
		$statement->execute();
		$homes = $statement->fetchAll();
		$statement->closeCursor();
		return $homes;
	}

?>

