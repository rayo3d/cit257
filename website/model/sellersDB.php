<?php
	require_once 'model/connectionDB.php';

	function getSeller ($sellerid) {

		$db = getConnection();
		$query = 'Select * From sellers Where sellerid = :sellerid';
		$statement = $db->prepare($query);
		$statement->bindValue(':sellerid', $sellerid);
		$statement->execute();
		$seller = $statement->fetch();
		$statement->closeCursor();
		return $seller;
	}

	function updateSeller ($sellerid, $firstname, $lastname, $email, $company) {

		$db = getConnection();
		$query = 'Update sellers Set firstname = :firstname, lastname = :lastname, email = :email, company = :company
			 Where sellerid = :sellerid';
		$statement = $db->prepare($query);
		$statement->bindValue(':firstname', $firstname);
		$statement->bindValue(':lastname', $lastname);
		$statement->bindValue(':email', $email);
		$statement->bindValue(':company', $company);
		$statement->bindValue(':sellerid', $sellerid);
		$statement->execute();
		$statement->closeCursor();
	}

	function addSeller ($firstname, $lastname, $email, $company) {

		$db = getConnection();
		$query = 'Insert Into sellers (firstname, lastname, email, company)
			 Values (:firstname, :lastname, :email, :company)';
		$statement = $db->prepare($query);
		$statement->bindValue(':firstname', $firstname);
		$statement->bindValue(':lastname', $lastname);
		$statement->bindValue(':email', $email);
		$statement->bindValue(':company', $company);
		$statement->execute();
		$statement->closeCursor();
	}

	function deleteSeller ($sellerid) {

		$db = getConnection();
		$query = 'Delete From sellers Where sellerid = :sellerid';
		$statement = $db->prepare($query);
		$statement->bindValue(':sellerid', $sellerid);
		$statement->execute();
		$statement->closeCursor();
	}

	function getSellers () {

		$db = getConnection();
		$query = 'Select * From sellers';
		$statement = $db->prepare($query);
		$statement->execute();
		$sellers = $statement->fetchAll();
		$statement->closeCursor();
		return $sellers;
	}

?>

