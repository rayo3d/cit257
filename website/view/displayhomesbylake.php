<?php include 'templates/header.php'; ?>

<?php include 'templates/navigation.php'; ?>

<div id="content" class="maincontent">
    <?php
        print '<h1>Homes on '
        . $lakeName
        . '</h1>';
    
        if (empty($homes)) {
            print '<p>No homes currently for sale on this lake.</p>';
        } else {
            foreach ($homes as $home){
                print '<p>'
                . '<a href="homeinfo.php?homeid='
                . $home['homeid'] . ' ">$'
                . number_format($home['price'], 2) . ' home at '
                . $home['address'] . ', '
                . $home['city'] . ', '
                . $home['state'] . '</a>'
                . '</p>';
            }
        }
    ?>
</div>
   
<?php include 'templates/footer.php'; ?>