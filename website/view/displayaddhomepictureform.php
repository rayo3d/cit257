<?php include 'templates/header.php'; ?>

<?php include 'templates/navigation.php'; ?>

<div id="content" class="maincontent">
    <h1>Add a Home Picture</h1>
    
    <p>Choose a picture file to upload (only .jpg images please).</p>
    
    <form action="addhomepicture.php" method="post" enctype="multipart/form-data">
        
        <input type="hidden" name="MAX_FILE_SIZE" value="500000">
        
        <?php print '<input type="hidden" name="home_id" value="' . $homeid . '">'; ?>
        
        <input name="uploadfile" type="file"><br><br>
        
        <input type="submit" name="submit" value="Submit">
    
    </form>
</div>
   
<?php include 'templates/footer.php'; ?>