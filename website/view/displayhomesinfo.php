<?php include 'templates/header.php'; ?>

<?php include 'templates/navigation.php'; ?>

<div id="content" class="maincontent">
    <h1>Home Details</h1>
    
    <h2>
        <?php print '$' . number_format($home['price'], 2); ?>
    </h2>
    
    <p>
        <?php print $home['location']; ?>
    </p>
    
    <p>
        <?php print $home['lakename']; ?>
    </p>
    
    <p>
        <?php print $home['address']; ?>
    </p>
    
    <p>
        <?php print 
            $home['city'] . ', '
            . $home['state'] . ' '
            . $home['zip'];
        ?>
    </p>
    
    <p>
        <?php print $home['description']; ?>
    </p>
    
    <p>
        <?php print $home['squarefeet'] . ' squarefeet'; ?>
    </p>
    
    <p>
        <?php print $home['bedrooms'] . ' bedrooms'; ?>
    </p>
    
    <p>
        <?php print $home['bathrooms'] . ' bathrooms'; ?>
    </p>
    
    <p>
        <?php print 'Seller: ' . $home['firstname'] . ' ' . $home['lastname'] . ' - ' . $home['company']; ?>
    </p>
    
    <p>
        <?php print '<a href="picturesbyhome.php?homeid=' . $homeid . '">Pictures</a>'; ?>
    </p>
</div>
   
<?php include 'templates/footer.php'; ?>