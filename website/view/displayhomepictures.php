<?php include 'templates/header.php'; ?>

<?php include 'templates/navigation.php'; ?>

<div id="content" class="maincontent">
    <?php
        if (empty($homepics)) {
            print '<p>No pictures currently for this home.</p>';
        } else {
            foreach ($homepics as $pic){
                print '<div class="homepicture"><img src="images/'
                . $pic['homepicture']
                . '" alt="Home Picture" />'
                . '</div>';
            }
        }
    
        print '<p><a href="addhomepictureform.php?homeid=' . $homeid . '">Add Home Picture</a></p>';
    ?>
</div>
   
<?php include 'templates/footer.php'; ?>