<?php include 'templates/header.php'; ?>

<?php include 'templates/navigation.php'; ?>

<div id="content" class="maincontent">
    <h1>
        Welcome to Kentucky Lake Homes
    </h1>

    <p>
        The Kentucky Lakes Realty Association (KLRA) promotes tourism and home sales on Kentucky&#39;s many lakes.
    </p>

    <p>
        The KLRA has decided to create this web site to bring Kentucky lake home buyers and sellers to a central site, regardless of lake, realty company, etc.
    </p>
    
    <h2>
        Please select at lake to see homes for sale on that lake.
    </h2>
    
    <?php
        foreach ($lakes as $lake){
            print '<p>'
            . '<a href="homesbylake.php?lakeid='
            . $lake['lakeid'] . ' ">'
            . $lake['lakename'] . '</a>'
            . '</p>';
        }
    ?>
</div>
   
<?php include 'templates/footer.php'; ?>