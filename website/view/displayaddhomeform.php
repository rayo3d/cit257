<?php include 'templates/header.php'; ?>

<?php include 'templates/navigation.php'; ?>

<div id="content" class="maincontent">
    <h1>
        New Home Form
    </h1>

    <form action="addhome.php" method="post">
        <label for="address">Address:</label><input type="text" name="address"><br>
        <label for="city">City:</label><input type="text" name="city"><br>
        <label for="state">State:</label><input type="text" name="state"><br>
        <label for="zip">Zip:</label><input type="text" name="zip"><br>
        <label for="description">Description:</label><textarea name="description" rows="5" cols="30"></textarea><br>
        <label for="bedrooms">Bedrooms:</label><input type="text" name="bedrooms"><br>
        <label for="bathrooms">Bathrooms:</label><input type="text" name="bathrooms"><br>
        <label for="squarefeet">Square Feet:</label><input type="text" name="squarefeet"><br>
        <label for="price">Price:</label><input type="text" name="price"><br>

        <label for="lakeid">Lake:</label><select name="lakeid">
            <option value="0">Please select a lake</option>
            <?php
                foreach ($lakes as $lake){
                    print '<option value="'
                    . $lake['lakeid'] . '">'
                    . $lake['lakename'] . '</option>';
                }
            ?>
        </select><br>

        <label for="locationid">Location:</label><select name="locationid">
            <option value="0">Please select a location</option>
            <?php
                foreach ($locations as $location){
                    print '<option value="'
                    . $location['locationid'] . '">'
                    . $location['location'] . '</option>';
                }
            ?>
        </select><br>

        <label for="sellerid">Seller:</label><select name="sellerid">
            <option value="0">Please select a seller</option>
            <?php
                foreach ($sellers as $seller){
                    print '<option value="'
                    . $seller['sellerid'] . '">'
                    . $seller['firstname'] . ' '
                    . $seller['lastname'] . '</option>';
                }
            ?>
        </select><br><br>

        <input type="submit" name="submit" value="Submit">
    </form>
</div>
   
<?php include 'templates/footer.php'; ?>