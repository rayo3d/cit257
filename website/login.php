<?php
    session_start();
    
    require_once 'iohelpers/input.php';
    
    try {
        $input = new Input();
        
        $username = $input->Text('username', 'Please enter a username');
        $password = $input->Text('password', 'Please enter a password');
        
        if (!empty($input->Errors)) { 
            print $input->FormattedErrMsg();
            exit;
        }
        
        if ($username == 'admin' && $password == 'kira'){
            $_SESSION['username'] = $username;
            
            header("Location: index.php");
        } else {
            exit("Invalid username or password");
        }
    } catch (Exception $ex) {
        exit ($ex->getMessage());
    }
?>
