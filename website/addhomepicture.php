<?php
    session_start();
                
    //If the username is not in the $_SESSION array
    if (empty($_SESSION['username'])) {
        //Exit with an error message
        exit("You must be logged in to perform this function.");
    }

    //If the username is not 'admin'
    if ($_SESSION['username'] != 'admin') { 
        //Exit with an error message
        exit ("You are not allowed to perform this function");
    }

    require_once 'model/homepicturesDB.php';
    require_once 'iohelpers/input.php';
    require_once 'iohelpers/upload.php';

    try {
        // Capture user input from add home form
        $input = new Input();
        
        $homeid = $input->Text('home_id', 'Please provide home_id');
//        $maxfilesize = $input->Text('MAX_FILE_SIZE', 'Please provide max file size');
//        $uploadfile = $input->File('uploadfile', 'Please select an image');

        if (!empty($input->Errors)) { 
            print $input->FormattedErrMsg(); 
            exit; 
        }
        
        try {
            // Get homeid (minimum accetable value of 1
            $picturefile = uploadJPG('uploadfile', 'images');
        } catch (Exception $ex) {
            exit('Home Picture Upload Exception: '
                . $ex->getMessage());
        }
        
        $filelink = 'images/' . $picturefile;
        
        addHomepic($picturefile, $filelink, $homeid);

        header("Location: homeinfo.php?homeid=" . $homeid);
        exit;
    } catch (Exception $ex) {
        exit('Add Home Exception: '
            . $ex->getMessage());
    }
?>