<?php
    session_start();
                
    //If the username is not in the $_SESSION array
    if (empty($_SESSION['username'])) {
        //Exit with an error message
        exit("You must be logged in to perform this function.");
    }

    //If the username is not 'admin'
    if ($_SESSION['username'] != 'admin') { 
        //Exit with an error message
        exit ("You are not allowed to perform this function");
    }

    require_once 'model/homesDB.php';
    require_once 'model/lakesDB.php';
    require_once 'model/locationsDB.php';
    require_once 'model/sellersDB.php';
    require_once 'iohelpers/input.php';

    try {
        // Capture user input from add home form
        $input = new Input();
        
        $address = $input->Text('address', 'Please enter an address');
        $city = $input->Text('city', 'Please enter the city');
        $state = $input->Text('state', 'Please enter the state');
        $zip = $input->Integer('zip', 'Please enter the zip code', 1);
        $description = $input->Text('description', 'Please enter a description');
        $bedrooms = $input->Integer('bedrooms', 'Please enter the number of bedrooms', 1);
        $bathrooms = $input->Integer('bathrooms', 'Please enter the number of bathrooms', 1);
        $squarefeet = $input->Integer('squarefeet', 'Please enter the square footage', 1);
        $price = $input->Number('price', 'Please enter the list price', .01);

        $lakeId = $input->Integer('lakeid', 'Please select a lake', 1);
        $locationId = $input->Integer('locationid', 'Please select a location', 1);
        $sellerId = $input->Integer('sellerid', 'Please select a seller', 1);

        if (!empty($input->Errors)) { 
            print $input->FormattedErrMsg(); 
            exit; 
        }
        
        $lakeName = getLake($lakeId);
        $locationName = getLocation($locationId);
        $sellerInfo = getSeller($sellerId);
        $sellerName = $sellerInfo['firstname'] . ' ' . $sellerInfo['lastname'];
        
        addHome($address, $city, $state, $zip, $description, $bedrooms, $bathrooms, $squarefeet, $price, $lakeName['lakename'], $locationName['location'], $sellerName, $locationId, $sellerId, $lakeId);

        header("Location: homesbylake.php?lakeid=" . $lakeId);
        exit;
    } catch (Exception $ex) {
        exit('Add Home Exception: '
            . $ex->getMessage());
    }
?>