<?php
    function uploadJPG($inputname, $folder, $maxsize=500000) {
        //This function uploads a jpg file
        
        //Argument $inputname is the name of the form's file input control
        //Argument $folder is the folder where 
        //the uploaded jpg file should be placed
        
        //This function returns the name of the uploaded file
        
        //This function might throw an exception, 
        //so be sure to use it within a try - catch block

        //Handle any file upload errors
        if ($_FILES[$inputname]['error'] > 0){

                switch ($_FILES[$inputname]['error']){
                        case 1: $errmsg =  "File exceeded upload_max_filesize";
                                        break;
                        case 2: $errmsg =  "File exceeded max_file_size";
                                        break;
                        case 3: $errmsg =  "File only partially uploaded";
                                        break;
                        case 4: $errmsg =  "No file uploaded";
                                        break;
                        case 6: $errmsg =  "Cannot upload file: No temp directory specified";
                                        break;
                        case 7: $errmsg =  "Upload failed: Cannot write to disk";
                                        break;
                        default: $errmsg = "Undetermined file upload error";
                }
                throw new Exception($errmsg);
        }

        //Make sure the file is not too big
        if ($_FILES[$inputname]['size'] > $maxsize) {
                throw new  Exception("File size too large");
        }

        //Make sure the browser sent a jpeg image
        if ($_FILES[$inputname]['type'] != 'image/jpeg' 
                && $_FILES[$inputname]['type'] != 'image/pjpeg') {
                        throw new  Exception("File type not image/jpeg");
        }

        //Make sure that PHP thinks the file is a jpeg image
        if (exif_imagetype($_FILES[$inputname]['tmp_name']) != IMAGETYPE_JPEG) {
                throw new  Exception("File type not image/jpeg");
        }

        //Make sure the file was uploaded from the user's machine 
        //and is not a file on the server
        if (!is_uploaded_file($_FILES[$inputname]['tmp_name'])) {
                throw new Exception("Invalid File");
        }

        //Make sure a filename has been supplied
        if (empty($_FILES[$inputname]['name'])) {
                throw new Exception("No file selected");
        }

        //Retrieve just the file's name without any path or extension
        $info = pathinfo($_FILES[$inputname]['name']);
        $newfilename = $info['filename'];

        //Give the file an extension of '.jpg'
        $newfilename = $newfilename . '.jpg';

        //Make sure the filename does not already exist in the "images" folder
        if (file_exists($folder . '/' . $newfilename)) {
                throw new Exception("File already exists");
        }

        //If moving the uploaded file to the $folder folder is successful
        if (move_uploaded_file($_FILES[$inputname]['tmp_name'], $folder . '/' . $newfilename)) {
                //Return the name of the newly uploaded jpg file
                return $newfilename;
        } else {
                //Otherwise, throw an exception
                throw new Exception("Error processing file");
        }
    }
?>
