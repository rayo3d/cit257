<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kentucky Lake Homes</title>
        <link href="templates/css/mainstyle.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page">
            <?php
                require_once 'model/lakesDB.php';

                try {
                    $lakes = getLakes();
                    
                    include 'view/displayhomepage.php';
                    exit();
                } catch (Exception $ex){
                    exit ('Exception: ' . $ex->getMessage());
                }
            ?>
        </div>
    </body>
</html>
